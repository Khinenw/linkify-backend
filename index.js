const bodyParser = require('body-parser')
const createAsyncRouter = require('@khinenw/express-async-router');
const express = require('express');
const { Sequelize, Model, DataTypes } = require('sequelize');

const sequelize = new Sequelize(
	process.env.DB_NAME,
	process.env.DB_USERNAME,
	process.env.DB_PASSWORD,
	{
		host: process.env.DB_HOST,
		dialect: 'postgres'
	}
);

class Link extends Model {}
Link.init({
	name: DataTypes.STRING,
	url: DataTypes.TEXT
}, { sequelize });

const app = express();
app.use(bodyParser.json());

const router = createAsyncRouter();

router.use(async (req, res, next) => {
	if (req.get('Authorization') !== `Bearer ${process.env.TOKEN}`) {
		res.status(403).json({
			ok: false,
			reason: 'authorization-failed'
		});
		return;
	}
	
	next();
});


router.get('/', (req, res) => {
	res.status(419).json({
		ok: true,
		me: 'teapot'
	});
});

router.get('/links', async (req, res) => {
	const links = await Link.findAll({});
	res.json({
		ok: true,
		links: links.map(({ name, url }) => ({ name, url }))
	});
});

router.post('/links/:name([A-Za-z0-9-_]+)', async (req, res) => {
	const name = req.params.name;
	const url = req.body.url;
	
	let urlParsed;
	try {
		urlParsed = (new URL(url)).toString();
	} catch(err) {
		res.status(422).json({
			ok: false,
			reason: 'wrong-url'
		});
		return;
	}
	
	if (await Link.findOne({ where: { name } })) {
		res.status(422).json({
			ok: false,
			reason: 'already-exists'
		});
		return;
	}
	
	await Link.create({
		name,
		url
	});
	
	res.json({
		ok: true
	});
});

router.delete('/links/:name([A-Za-z0-9-_]+)', async (req, res) => {
	const deleted = await Link.destroy({
		where: { name: req.params.name }
	});
	
	if (deleted === 0) {
		res.status(404).json({
			ok: false,
			reason: 'not-found'
		});
		return;
	}
	
	res.json({
		ok: true
	});
});

app.use('/api', router);

app.get('/:name([A-Za-z0-9-_]+)', (req, res, next) => {
	Link
		.findOne({
			where: { name: req.params.name }
		})
		.then(link => {
			if (link) {
				res.redirect(link.url);
				return;
			}
			
			res.status(404).send('404 Not Found T_T');
		})
		.catch(err => {
			next(err);
		});
});

app.use((err, req, res, next) => {
	res.status(500).send('500ISE T_T');
});

(async () => {
	await sequelize.sync();
	
	const port = parseInt(process.env.PORT || '3008');
	app.listen(port);
	console.log("Listening!");
})();
